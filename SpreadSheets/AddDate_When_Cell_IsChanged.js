// Created by Mihai (https://gitlab.com/Mihai-MCW) on 05/06/2019
// This file is meant to change the value of cell when another cell is modified, 
// depending on the row and the value that it was modified into

function onEdit(event){
  // Set the app variable
  var app = SpreadsheetApp;
  // Get the active sheet
  var activeSheet = app.getActiveSpreadsheet().getActiveSheet();
  // Set the sheets of interest
  var targetSheet = ["Sheet2","Sheet3"]; // Names of the sheets on which the function will react (should be changed)
  // Set the interest variable
  var isOfInterest = false;
  
  // Check if the active sheet is part of the interest sheets
  for(var i = 0; i < targetSheet.length; i++){
    if (activeSheet.getName() == targetSheet[i]){isOfInterest = true;}
  }
  // Check if the sheet was found of interest
  if(isOfInterest){
    // Reset the interest variable for the next check
    isOfInterest = false;
    // Set the colums on which to react
    var reactCol = [1,3,6]; // This is a column setting. The code has to be modified to make a row setting. Those are the colums on which I follow if data is changed
    // Get the active range from the event
    var activeRange = event.range;
    // getRow getColumn getLastRow getLastColumn 
    
    // Initialize the list of indexes of columns of interest (reactCol)
    var index = []; // Stays empty
    
    // Check if the column is part of the interest columns (reactCol)
    for(var i = 0; i < reactCol.length; i++){
      // Check if the colulmn is of interest
      if (activeRange.getColumn() <= reactCol[i] && activeRange.getLastColumn() >= reactCol[i]){
        isOfInterest = true;
        index.push(i);
      }
    }
    
    // Check if any columns were found of interest
    if(isOfInterest){
      // Set the offset variables
      var offsetX = [1,1,2]; // Cell offset variables should be changed based on the offset for the new cell to be filled
      var offsetY = [0,0,0]; // This cell offset is for the row offset, and normally would be 0 in a column setting like this
      // Set the value to check against for each column
      var checkValue = ["t1","t2","t3"]; // this could be transformed into a 2D matrix to hold multiple values which will determine later what value to fill and in what cell
      
      // Loop through all the found columns (their indexes)
      for(var j = 0; j < index.length; j++){
        // Loop through all the rows
        for(var i = activeRange.getRow(); i <= activeRange.getLastRow();i++){
          // Check each checkValue with each row, depending on its correspondant column
          if(checkValue[index[j]] == activeSheet.getRange(i,reactCol[index[j]]).getValue()){
            // Check if the cell to be filled is empty, otherwise it will not be filled with anything
            if(activeSheet.getRange(i+offsetY[index[j]],reactCol[index[j]]+offsetX[index[j]]).getValue() == ""){
              // Fill the date in the empty cell
              activeSheet.getRange(i+offsetY[index[j]],reactCol[index[j]]+offsetX[index[j]]).setValue(Utilities.formatDate(new Date(), "GMT+2", "dd/MM/yyyy, HH:mm"));
              //activeSheet.getRange(i+offsetY[index[j]],reactCol[index[j]]+offsetX[index[j]]).setValue(new Date());
              //activeSheet.getRange(i+offsetY[index[j]],reactCol[index[j]]+offsetX[index[j]]).setValue(Date());
            }
          }
        }
      } 
    } 
  }
}


